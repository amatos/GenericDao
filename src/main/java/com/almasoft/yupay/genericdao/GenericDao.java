/*
 * This software is property of Almasoft
 * by Alejandro Matos
 * Lima - Perú
 */
package com.almasoft.facturapp.dao.interfaces;

import com.almasoft.facturapp.throwable.SQLException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Alejandro Matos<amatos@gmail.com>
 * @param <T>
 */
public interface GenericDao<T>
{
    /**
     * Persist the given transient instance, first assigning a generated identifier
     *
     * @param t
     * @return
     */
    @Transactional
    Serializable save(final T t);

    /**
     * Persist the given transient instances, first assigning a generated identifier
     *
     * @param list
     * @throws SQLException
     */
    void save(final List<T> list) throws SQLException;

    /**
     * Return the persistent instance of the given entity class with the given identifier, assuming that the instance exists.
     *
     * @param id
     * @return
     */
    T load(final Serializable id);

    /**
     * Return all the persistent instances of the given entity class
     * Not usable when working with multicompany, must check.
     * @return
     */
    List<T> findAll();

    /**
     * Return the first persistent instances of the given entity class
     * Not usable when working with multicompany, must check.
     * @param amount
     * @return
     */
    List<T> findFirst(final int amount);

    /**
     * Return all the persistent instances of the given entity class between a range
     * Not usable when working with multicompany, must check.
     *
     * @param from
     * @param to
     * @return
     */
    List<T> findInRange(final int from, final int to);

    /**
     * Update the persistent instance with the identifier of the given detached instance.
     *
     * @param t
     */
    void update(final T t);

    /**
     * Remove a persistent instance from the datastore. Some of the instances will
     * be edited to set the field <i>active</i> to false
     *
     * @param t
     */
    void delete(final T t);

    /**
     * Remove the persistent instances from the datastore.
     * @param list 
     */
    void delete(final List<T> list);

    /**
     * Return true or false depending if the persistent entity exists.
     * @param id
     * @return 
     */
    boolean exists(final Serializable id);
    
    public List<T> findActive();
    
    public List<T> findInactive();
            
    /**
     * Returns true if <b>ANY</b> of the parameters exist in the <code>table</code>
     * Used when trying to check multiple fields. In case you want to check only <b>one</b> field, use {@link GenericDao#doesParameterExists(java.lang.String, java.lang.String, java.lang.String) doesParameterExists}
     * @param paramValues Map with the param name <b>AND</b> value.
     * @param table Where are we searching for this value?
     * @return 
     */
    public boolean doesParameterExists(final Map<String, String> paramValues, final String table);
    
    /**
     * Returns true if there is a <code>field</code> with the given <code>value</code> in the <code>table</code>. 
     * Used when checking only one field. In case you want to check more then one, use {@link GenericDao#doesParameterExists(java.util.Map, java.lang.String) doesParameterExists}
     * @param table Where the check is being performed.
     * @param parameter The parameter's name.
     * @param value The value for the parameter.
     * @return 
     */
    public boolean doesParameterExists(final String table, final String parameter, final String value);
}
