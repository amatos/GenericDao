/*
 * This software is property of Almasoft
 * by Alejandro Matos
 * Lima - Perú
 */
package com.almasoft.facturapp.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.almasoft.facturapp.dao.interfaces.GenericDao;
import com.almasoft.facturapp.throwable.SQLException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Alejandro Matos<amatos@gmail.com>
 * @param <T>
 */
@Transactional
public abstract class GenericDaoImpl<T> implements GenericDao<T>
{
    @Autowired
    protected SessionFactory sessionFactory;

    Class<T> clazz;

    public GenericDaoImpl()
    {
        this.clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    @Transactional
    public Serializable save(T t)
    {
        return getCurrentSession().save(t);
    }

    @Override
    @Transactional
    public void save(List<T> list) throws SQLException
    {
        list.stream().forEach((item)
                -> 
                {
                    getCurrentSession().save(item);
        });
    }

    @Override
    public T load(Serializable id)
    {
        return (T) getCurrentSession().load(clazz, id);
    }

    @Override
    public List<T> findAll()
    {
        // TODO: ese 100 deberá salir de la configuración de la empresa con su advertencia.
        // TODO: Por defecto debería ser 100
        List list = createQuery("from  " + clazz.getSimpleName()).setMaxResults(100).list();
        return list;
    }

    @Override
    public List<T> findFirst(int amount)
    {
        return createQuery("from  " + clazz.getSimpleName()).setMaxResults(amount).list();
    }

    @Override
    @Transactional
    public void update(T t)
    {
        getCurrentSession().update(t);
    }

    @Override
    public void delete(T t)
    {
        getCurrentSession().delete(t);
    }

    @Override
    @Transactional
    public void delete(List<T> list)
    {
        list.stream().forEach((item)
                -> 
                {
                    this.delete(item);
        });
    }

    @Override
    public List<T> findInRange(int from, int to)
    {
        return createQuery("from  " + clazz.getSimpleName()).setFirstResult(from).setMaxResults(to).list();
    }

    @Override
    public boolean exists(Serializable id)
    {
        T t = load(id);
        return t == null;
    }

    @Override
    public boolean doesParameterExists(final Map<String, String> paramValues, final String table)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT count(*) FROM ")
                .append(table)
                .append(" t WHERE ");
        paramValues.forEach((k, v) -> sb.append("t.").append(k).append(" =:").append(v).append(" and "));
        
        /************************************************************/
        /* this is necesary to get rid of the last AND in the query */
        /* Do you have a better idea? bring it on, bitch!           */
        /************************************************************/
        
        // FIXME: find a better way...
        sb.replace(sb.length() - 5, sb.length(), "");
        String substring = sb.toString().substring(0, sb.length() - 5);

        Query query = createQuery(substring);
        Long count = (Long) query.uniqueResult();
        return count > 0;
    }
    
    @Override
    public boolean doesParameterExists(final String table, final String parameter, final String value)
    {
        Query query = createQuery("SELECT count(*) from "+table+" WHERE "+parameter+" = :"+value);
        Long count = (Long) query.uniqueResult();
        return count > 0;
    }

    @Override
    public List<T> findActive()
    {
        return createQuery("from  " + clazz.getSimpleName() + " t where t.active = true").setMaxResults(100).list();
    }

    @Override
    public List<T> findInactive()
    {
        return createQuery("from  " + clazz.getSimpleName() + " t where t.active = false").setMaxResults(100).list();
    }

    public Query createQuery(String query)
    {
        return getCurrentSession().createQuery(query);
    }

    public Query createSQLQuery(String query)
    {
        return getCurrentSession().createSQLQuery(query);
    }

    public Session getCurrentSession()
    {
        return this.sessionFactory.getCurrentSession();
    }

    public Transaction beginTransaction()
    {
        return this.getCurrentSession().beginTransaction();
    }
}
